// swift-tools-version: 5.8
import PackageDescription


let package = Package(
	name: "test-multiple-executables",
	platforms: [.macOS(.v12)],
//	products: [
//		.executable(name: "executable1", targets: ["executable1"]),
//		.executable(name: "executable2", targets: ["executable2"]),
//	],
	dependencies: [
		.package(url: "https://github.com/apple/swift-crypto.git",          from: "2.2.0"),
		.package(url: "https://github.com/apple/swift-package-manager.git", revision: "swift-5.8.1-RELEASE"),
	],
	targets: [
		.executableTarget(name: "executable1", dependencies: [
			.product(name: "SwiftPMDataModel", package: "swift-package-manager"),
			.product(name: "Crypto", package: "swift-crypto")
		]),
		.executableTarget(name: "executable2", dependencies: [
//			.product(name: "SwiftPMDataModel", package: "swift-package-manager"),
			.product(name: "Crypto", package: "swift-crypto")
		]),
	]
)
